%define         debug_package %{nil}

Name:           node_exporter
Version:        1.8.1
Release:        1
Summary:        Exporter for machine metrics
License:        Apache-2.0
URL:            https://github.com/prometheus/node_exporter
Source0:        https://github.com/prometheus/node_exporter/archive/v%{version}/%{name}-%{version}.tar.gz
# tar -xvf Source0
# run 'go mod vendor' in it
# tar -czvf node_exporter-vendor.tar.gz vendor
Source1:        node_exporter-vendor.tar.gz
Source2:        node_exporter.service
Source3:        node_exporter.sysconfig

BuildRequires:  systemd
BuildRequires:  promu
BuildRequires:  git
BuildRequires:  golang >= 1.18
Requires:       shadow coreutils
Provides:       golang-github-prometheus-node_exporter = %{version}-%{release}

ExclusiveArch:  x86_64 aarch64 riscv64 loongarch64

%description
Prometheus exporter for hardware and OS metrics exposed by *NIX kernels, written in Go with pluggable metric collectors.

%prep
%autosetup -p1 -n %{name}-%{version}
tar -xzvf %{SOURCE1}
sed -i '/flags:/ s/ -tags .*/ -tags '\''netgo osusergo static_build'\'' --mod=vendor/' .promu-cgo.yml .promu.yml

%build
promu build

%install
install -D -m 0755 node_exporter %{buildroot}%{_sbindir}/node_exporter
install -D -m 0644 %{SOURCE2} %{buildroot}%{_unitdir}/node_exporter.service
install -D -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/sysconfig/node_exporter

%pre
getent group node_exporter > /dev/null || groupadd -r node_exporter
getent passwd node_exporter > /dev/null || \
    useradd -rg node_exporter -d /var/lib/node_exporter -s /sbin/nologin \
            -c "Prometheus node exporter" node_exporter

%post
%systemd_post node_exporter.service

%preun
%systemd_preun node_exporter.service

%postun
%systemd_postun node_exporter.service

%files
%doc README.md
%license LICENSE
%{_sbindir}/node_exporter
%{_unitdir}/node_exporter.service
%config(noreplace)%{_sysconfdir}/sysconfig/node_exporter

%changelog
* Thu Jul 11 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 1.8.1-1
- Update package to version 1.8.1
- Fix debug log in cpu collector
- Fix pressure collector nil reference

* Fri Jun 21 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 1.7.0-4
- Add new collector and metrics for watchdog

* Tue Apr 23 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 1.7.0-3
- Fix the problem of failure to start node_exporter service

* Tue Feb 27 2024 Huang Yang <huangyang@loongson.cn> - 1.7.0-2
- add loongarch64 support

* Fri Dec 29 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 1.7.0-1
- Update package to version 1.7.0

* Wed Feb 15 2023 laokz <zhangkai@iscas.ac.cn> - 1.0.1-3
- Add runtime requires: shadow coreutils
- Backport 1.1.0(procfs-0.3.0) patches to support riscv64

* Sat Feb 20 2021 yangzhao <yangzhao1@kylinos.cn> - 1.0.1-2
- Remove unnecessary requirements

* Wed Sep 17 2020 houjian <houjian@kylinos.cn> - 1.0.1-1
- Package Init
